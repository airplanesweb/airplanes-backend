FROM python:3.7

RUN mkdir /src

ADD src/Pipfile /src
ADD src/Pipfile.lock /src

RUN pip install pipenv
WORKDIR /src
RUN pipenv install --system --deploy --ignore-pipfile
# ADD src /src

EXPOSE 8000
CMD ["python", "./manage.py", "runserver", "0.0.0.0:8000"]
