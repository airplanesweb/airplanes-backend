from rest_framework import generics, status
from rest_framework.views import APIView
from rest_framework.response import Response
from main.models import *
from main.serializers import *
import datetime
from django.shortcuts import render


def index(request):
    return render(request, 'index.html', {})


# ==============
# CITIES
# ==============

class CityList(APIView):
    """
    List and create cities

    Query params:
    @name(optional): Return all cities with name LIKE that
    """
    def get(self, request):  
        if request.query_params:
            name = request.query_params['name']
            if len(name) < 3:
                return Response({})
            cities = City.objects.filter(name__icontains=name)[:6]
        else:
            cities = City.objects.select_related().all()
        serializer = CitySerializer(cities, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CitySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CityDetails(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, Update or Destroy a City entity
    """
    queryset = City.objects.all()
    serializer_class = CitySerializer


# ==============
# COUNTRIES
# ==============


class CountryList(generics.ListCreateAPIView):
    """
    List and create countries
    """
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class CountryDetails(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, Update or Destroy a Country entity
    """
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


# ==============
# AIRPORTS
# ==============


class AirportList(generics.ListCreateAPIView):
    """
    List and create airports
    """
    queryset = Airport.objects.all()
    serializer_class = AirportSerializer


class AirportDetails(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, Update or Destroy an Airport entity
    """
    queryset = Airport.objects.all()
    serializer_class = AirportSerializer


# ==============
# COMPANIES
# ==============


class CompanyList(generics.ListCreateAPIView):
    """
    List and create companies
    """
    queryset = Company.objects.all()
    serializer_class = CompanySerializer


class CompanyDetails(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, Update or Destroy a Company entity
    """
    queryset = Company.objects.all()
    serializer_class = CompanySerializer


# ==============
# FLIGHTS
# ==============


class FlightList(APIView):

    def get(self, request):
        validation = FlightSerializerExtended(data=self.request.query_params)
        if validation.is_valid() and validation.data:
            depart_id = validation.data['depart_id']
            arrive_id = validation.data['arrive_id']
            depart_date = validation.data['depart_date']
            comfort_class = validation.data['comfort_class']

            flights = Flight.objects.filter(comfort_class=comfort_class)

            arrive_date = None
            if 'arrive_date' in validation.data:
                arrive_date = validation.data['arrive_date']

            depart_date = datetime.datetime.strptime(depart_date, '%Y-%m-%d').date()

            flights = flights.filter(airport_depart__city=depart_id,
                                     airport_arrive__city=arrive_id,
                                     date_depart__range=(
                                         datetime.datetime.combine(depart_date, datetime.time.min),
                                         datetime.datetime.combine(depart_date, datetime.time.max)
                                     ))

            if arrive_date:
                arrive_date = datetime.datetime.strptime(arrive_date, '%Y-%m-%d').date()

                flights_back = Flight.objects.filter(comfort_class=comfort_class)
                flights_back = flights_back.filter(airport_depart__city=arrive_id,
                                                   airport_arrive__city=depart_id,
                                                   date_depart__range=(
                                                       datetime.datetime.combine(arrive_date, datetime.time.min),
                                                       datetime.datetime.combine(arrive_date, datetime.time.max)
                                                   ))
                print(flights_back)
                flights = flights | flights_back

            serializer = FlightSerializer(flights, many=True)
            return Response(serializer.data)

        return Response(validation.errors)


# ==============
# TICKETS
# ==============


class TicketList(generics.ListCreateAPIView):
    """
    List and create airports
    """
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer


class TicketDetails(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, Update or Destroy an Airport entity
    """
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer
