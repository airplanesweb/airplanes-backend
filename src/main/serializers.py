from rest_framework import serializers
from main.models import *


class CitySerializer(serializers.ModelSerializer):
    """
    Basic serializer for a City entity
    Has one-level depth for displaying a country object
    """
    class Meta:
        model = City
        fields = '__all__'
        depth = 1


class CitySerializerNoDepth(serializers.ModelSerializer):
    """
    Basic serializer for a City entity without depth
    """
    class Meta:
        model = City
        exclude = ['country']


class CountrySerializer(serializers.ModelSerializer):
    """
    Basic serializer for a Country object
    Additionally displays all related cities
    """
    cities = CitySerializerNoDepth(source='city_set', many=True, read_only=True)

    class Meta:
        model = Country
        fields = '__all__'


class AirportSerializer(serializers.ModelSerializer):
    """
    Basic serializer for an Airport entity
    Has two-level depth for displaying city in which airport is located
    and county of this city
    """
    class Meta:
        model = Airport
        fields = '__all__'
        depth = 2


class CompanySerializer(serializers.ModelSerializer):
    """
    Basic serializer for a Company object
    """
    class Meta:
        model = Company
        fields = '__all__'


class FlightSerializerExtended(serializers.Serializer):
    comfort_class = serializers.IntegerField()
    depart_id = serializers.IntegerField()
    arrive_id = serializers.IntegerField()

    depart_date = serializers.DateField()
    arrive_date = serializers.DateField(required=False)


class TicketSerializer(serializers.ModelSerializer):
    """
    Basic serializer for a Ticket entity
    """
    class Meta:
        model = Ticket
        fields = '__all__'


class FlightSerializer(serializers.ModelSerializer):
    """
    Basic serializer for a Flight entity
    """
    difference = serializers.JSONField()
    tickets = TicketSerializer(source="ticket_set", many=True, read_only=True)

    class Meta:
        model = Flight
        fields = '__all__'
        depth = 2
