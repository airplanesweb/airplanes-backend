from django.contrib import admin
from django.shortcuts import redirect
from main.models import *
# Register your models here.


class CityAdmin(admin.ModelAdmin):
    search_fields = ['name', 'code']
    autocomplete_fields = ['country']


class CountryAdmin(admin.ModelAdmin):
    search_fields = ['name', 'code']


class AirportAdmin(admin.ModelAdmin):
    search_fields = ['name']
    autocomplete_fields = ['city']


class CompanyAdmin(admin.ModelAdmin):
    search_fields = ['name']


class FlightAdmin(admin.ModelAdmin):
    search_fields = ['airport_depart', 'date_depart']
    autocomplete_fields = [
        'airport_depart',
        'airport_arrive',
        'companies'
    ]
    list_filter = ['baggage']


def print_out_ticket(modeladmin, request, queryset):
    return redirect('http://google.com')


print_out_ticket.short_description = 'Распечатать выбранные билеты'


class TicketAdmin(admin.ModelAdmin):
    search_fields = ['name']
    autocomplete_fields = [
        'flight'
    ]
    actions = [print_out_ticket]


admin.site.register(City, CityAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(Airport, AirportAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Flight, FlightAdmin)
admin.site.register(Ticket, TicketAdmin)

admin.site.site_header = 'Панель управления - "Moнитор"'
admin.site.site_title = 'Панель управления - "Moнитор"'
admin.site.index_title = 'Панель управления - "Moнитор"'
