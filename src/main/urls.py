from django.urls import path
from main.views import *

urlpatterns = [
    # CITIES
    path('cities/', CityList.as_view()),
    path('cities/<int:pk>', CityDetails.as_view()),
    # COUNTRIES
    path('countries/', CountryList.as_view()),
    path('countries/<int:pk>', CountryDetails.as_view()),
    # AIRPORTS
    path('airports/', AirportList.as_view()),
    path('airports/<int:pk>', AirportDetails.as_view()),
    # COMPANIES
    path('companies/', CompanyList.as_view()),
    path('companies/<int:pk>', CompanyDetails.as_view()),
    # FLIGHTS
    path('flights/', FlightList.as_view()),
    # TICKETS
    path('tickets/', TicketList.as_view()),
    path('tickets/<int:pk>', TicketDetails.as_view()),

]