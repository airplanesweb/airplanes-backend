# Generated by Django 2.2.1 on 2019-05-26 11:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20190525_2200'),
    ]

    operations = [
        migrations.AddField(
            model_name='ticket',
            name='child',
            field=models.BooleanField(default=False, verbose_name='Детское место'),
            preserve_default=False,
        ),
    ]
