import datetime
from django.db import models
from main.SMS.API import SmsAPI
import json
import pytz

# Create your models here.


class Country(models.Model):
    """
    Represents country entity

    @name: Name of the country
    @code: International code representing this country
    """
    name = models.CharField(max_length=210, verbose_name='Имя')
    code = models.CharField(max_length=5, verbose_name='Код')

    def __str__(self):
        return f'{self.name} - {self.code}'

    class Meta:
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'


class City(models.Model):
    """
    Represents city entity

    @name: Name of the city
    @code: International code representing this city
    @country: Country in which city is located
    """
    name = models.CharField(max_length=210, verbose_name='Имя')
    code = models.CharField(max_length=5, verbose_name='Код')
    country = models.ForeignKey(Country, on_delete=models.CASCADE, verbose_name='Страна')

    def __str__(self):
        return f'{self.name} - {self.code}'

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'


class Airport(models.Model):
    """
    Represents airport entity

    @name: Name of the airport
    @city: City in which airport is located
    """
    name = models.CharField(max_length=210, verbose_name='Имя')
    city = models.ForeignKey(City, on_delete=models.CASCADE, verbose_name='Город')

    def __str__(self):
        return f'{self.name} ({self.city.name} - {self.city.country.name})'

    class Meta:
        verbose_name = 'Аэропорт'
        verbose_name_plural = 'Аэропорты'


class Company(models.Model):
    """
    Represents company entity

    @name: Name of the company
    @logo: Logo of the company
    """
    name = models.CharField(max_length=210, verbose_name='Имя')
    logo = models.ImageField(verbose_name='Логотип')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Компания'
        verbose_name_plural = 'Компании'


class Flight(models.Model):
    """
    Represents flight entity

    @airport_depart: Depart from this airport
    @airport_arrive: Arrive to this airport
    @date_depart: Depart on this date
    @date_arrive: Arrive on this date
    @price: Price of this flight
    @baggage: With baggage or not
    @companies: Companies that offer this flight
    """
    airport_depart = models.ForeignKey(Airport, on_delete=models.CASCADE, verbose_name='Отправка из')
    airport_arrive = models.ForeignKey(Airport, on_delete=models.CASCADE,
                                       related_name='main_flight_related', verbose_name='Прибытие в')
    date_depart = models.DateTimeField(verbose_name='Дата отправления')
    date_arrive = models.DateTimeField(verbose_name='Дата прибытия')
    price = models.FloatField(verbose_name='Цена')
    companies = models.ManyToManyField(Company, verbose_name='Компании')
    ticket_amount = models.IntegerField(verbose_name='Количество билетов(мест)')
    comfort_class = models.SmallIntegerField(verbose_name='Класс', choices=(
        (0, 'Эконом'),
        (1, 'Бизнес')
    ))
    baggage = models.BooleanField(default=False, verbose_name='Багаж')

    @property
    def difference(self):
        difference_time_ = self.date_arrive-self.date_depart
        difference_time = difference_time_.total_seconds() / 3600 * 60

        hours = int(difference_time / 60)
        minutes = int(difference_time - (hours * 60))

        return {
            'hours': hours,
            'minutes': minutes,
            'total': difference_time_.total_seconds()
        }

    def __str__(self):
        return f'{self.airport_depart.name} - {self.airport_arrive.name} ({self.date_depart})'

    class Meta:
        verbose_name = 'Рейс'
        verbose_name_plural = 'Рейсы'


class Ticket(models.Model):
    """
    Represents ticket entity

    @flight: Ticker for this flight
    @created_at: Date of creation
    """
    flight = models.ForeignKey(Flight, on_delete=models.CASCADE, verbose_name='Рейс')
    full_name = models.CharField(max_length=220, verbose_name='ФИО')
    phone_number = models.CharField(max_length=20, verbose_name='Телефонный номер')
    passport = models.CharField(max_length=20, verbose_name='Паспорт')
    created_at = models.DateTimeField(auto_created=True, verbose_name='Дата покупки')
    row = models.IntegerField(verbose_name="Ряд")
    seat = models.IntegerField(verbose_name="Место")
    child = models.BooleanField(verbose_name="Детское место")

    def __str__(self):
        return f'{self.full_name} / {self.flight.__str__()}'

    class Meta:
        verbose_name = 'Билет'
        verbose_name_plural = 'Билеты'

    # Sending SMS with appropriate time
    def save(self, *args, **kwargs):
        super().save(self)
        krasnoyarsk = pytz.timezone('Asia/Krasnoyarsk')
        dd = self.flight.date_depart.astimezone(krasnoyarsk)

        date = dd.strftime('%d/%m')
        time = dd.strftime('%H:%M')
        message = f'Оплачено. Билет №{self.pk} на {date} в {time}'

        sms = SmsAPI(
            message=message,
            number=self.phone_number
        )
        response = sms.send()
        print(response)
